function parseMenu(rawMenu) {

    var mondayIndex = rawMenu.indexOf('PONIEDZIAŁEK');
    var tuesdayIndex = rawMenu.indexOf('WTOREK');
    var wednesdayIndex = rawMenu.indexOf('ŚRODA');
    var thursdayIndex = rawMenu.indexOf('CZWARTEK');
    var fridayIndex = rawMenu.indexOf('PIĄTEK');

    var monday = rawMenu.substring(mondayIndex,tuesdayIndex);
    var tuesday = rawMenu.substring(tuesdayIndex,wednesdayIndex);
    var wednsday = rawMenu.substring(wednesdayIndex,thursdayIndex);
    var thursday = rawMenu.substring(thursdayIndex,fridayIndex);
    var friday = rawMenu.substring(fridayIndex,rawMenu.length);

    var mondayZupy = monday.substring(monday.indexOf('ZUPY:')+6,monday.indexOf("KUBEK:")).split(', ');
    var mondayKubek = monday.substring(monday.indexOf('KUBEK:')+7,monday.indexOf("KUBEK FIT:")).split(', ');
    var mondayKubekFit = monday.substring(monday.indexOf('FIT:')+5,monday.indexOf("ATKI:") - 3).split(', ');
    var mondaySalatki = monday.substring(monday.indexOf('ATKI:')+6,monday.indexOf("DESERY:")).split(', ');
    var mondayDesery = monday.substring(monday.indexOf('DESERY:')+8,monday.length).split(', ');

    var tuesdayZupy = tuesday.substring(tuesday.indexOf('ZUPY:')+6,tuesday.indexOf("KUBEK:")).split(', ');
    var tuesdayKubek = tuesday.substring(tuesday.indexOf('KUBEK:')+7,tuesday.indexOf("KUBEK FIT:")).split(', ');
    var tuesdayKubekFit = tuesday.substring(tuesday.indexOf('FIT:')+5,tuesday.indexOf("ATKI:") - 3).split(', ');
    var tuesdaySalatki = tuesday.substring(tuesday.indexOf('ATKI:')+6,tuesday.indexOf("DESERY:")).split(', ');
    var tuesdayDesery = tuesday.substring(tuesday.indexOf('DESERY:')+8,tuesday.length).split(', ');

    var wednsdayZupy = wednsday.substring(wednsday.indexOf('ZUPY:')+6,wednsday.indexOf("KUBEK:")).split(', ');
    var wednsdayKubek = wednsday.substring(wednsday.indexOf('KUBEK:')+7,wednsday.indexOf("KUBEK FIT:")).split(', ');
    var wednsdayKubekFit = wednsday.substring(wednsday.indexOf('FIT:')+5,wednsday.indexOf("ATKI:") - 3).split(', ');
    var wednsdaySalatki = wednsday.substring(wednsday.indexOf('ATKI:')+6,wednsday.indexOf("DESERY:")).split(', ');
    var wednsdayDesery = wednsday.substring(wednsday.indexOf('DESERY:')+8,wednsday.length).split(', ');

    var thursdayZupy = thursday.substring(thursday.indexOf('ZUPY:')+6,thursday.indexOf("KUBEK:")).split(', ');
    var thursdayKubek = thursday.substring(thursday.indexOf('KUBEK:')+7,thursday.indexOf("KUBEK FIT:")).split(', ');
    var thursdayKubekFit = thursday.substring(thursday.indexOf('FIT:')+5,thursday.indexOf("ATKI:") - 3).split(', ');
    var thursdaySalatki = thursday.substring(thursday.indexOf('ATKI:')+6,thursday.indexOf("DESERY:")).split(', ');
    var thursdayDesery = thursday.substring(thursday.indexOf('DESERY:')+8,thursday.length).split(', ');

    var fridayZupy = friday.substring(friday.indexOf('ZUPY:')+6,friday.indexOf("KUBEK:")).split(', ');
    var fridayKubek = friday.substring(friday.indexOf('KUBEK:')+7,friday.indexOf("KUBEK FIT:")).split(', ');
    var fridayKubekFit = friday.substring(friday.indexOf('FIT:')+5,friday.indexOf("ATKI:") - 3).split(', ');
    var fridaySalatki = friday.substring(friday.indexOf('ATKI:')+6,friday.indexOf("DESERY:")).split(', ');
    var fridayDesery = friday.substring(friday.indexOf('DESERY:')+8,friday.length).split(', ');

    var menuJSON = {
        PON: {
            ZUPY: mondayZupy,
            KUBEK: mondayKubek,
            KUBEK_FIT: mondayKubekFit,
            SALATKI: mondaySalatki,
            DESERY: mondayDesery
        },
        WT: {
            ZUPY: tuesdayZupy,
            KUBEK: tuesdayKubek,
            KUBEK_FIT: tuesdayKubekFit,
            SALATKI: tuesdaySalatki,
            DESERY: tuesdayDesery
        },
        SR: {
            ZUPY: wednsdayZupy,
            KUBEK: wednsdayKubek,
            KUBEK_FIT: wednsdayKubekFit,
            SALATKI: wednsdaySalatki,
            DESERY: wednsdayDesery
        },
        CZW: {
            ZUPY: thursdayZupy,
            KUBEK: thursdayKubek,
            KUBEK_FIT: thursdayKubekFit,
            SALATKI: thursdaySalatki,
            DESERY: thursdayDesery
        },
        PT: {
            ZUPY: fridayZupy,
            KUBEK: fridayKubek,
            KUBEK_FIT: fridayKubekFit,
            SALATKI: fridaySalatki,
            DESERY: fridayDesery
        }
    };

    console.log(menuJSON);
    return menuJSON;
}

function sendmenu(menuJson) {
    AJS.$.ajax({
        url: AJS.contextPath() + "/rest/slimak/latest/Food/menu",
        type: "POST",
        dataType: "text",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify(menuJson),
        async: false,
        cache: false,
        success: function (data) {
            console.log('poszlo');
            AJS.dialog2("#demo-dialog").hide();
        },
        error: function (data) {
            console.log('nie poszloo');
            AJS.dialog2("#demo-dialog").hide();
        }
    });
}

AJS.$(document).ready(function () {
    AJS.$("#dialog-add-menu").click(function () {
        AJS.dialog2("#demo-dialog").show();
    });
    AJS.$("#dialog-submit-button").click(function () {
        var menuJSON = parseMenu(AJS.$('#textarea-id').val());
        sendmenu(menuJSON);
    });
    AJS.$("#dialog-close-button").click(function () {
        AJS.dialog2("#demo-dialog").hide();
    });
});