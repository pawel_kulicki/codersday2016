//getdata
var todayData = [
];

var tomorrowData = [
];

AJS.$(document).ready(function () {

    AJS.$.ajax({
        "type": "GET",
        "url": AJS.contextPath() + "/rest/slimak/latest/Food/dishes",
        "dataType": "json",
        "async": false,
        success: function(data) {
            var currentDay = new Date().getDay();
            _.each(data, function (dish) {
                switch (dish.dayOfWeek){
                    case "PON":
                        dish.dayOfWeek = 1;
                        break;
                    case "WT":
                        dish.dayOfWeek = 2;
                        break;
                    case "SR":
                        dish.dayOfWeek = 3;
                        break;
                    case "CZW":
                        dish.dayOfWeek = 4;
                        break;
                    case "PT":
                        dish.dayOfWeek = 5;
                        break;
                }

                if(dish.dayOfWeek == currentDay){
                    todayData.push(dish);
                }

                if(dish.dayOfWeek == (currentDay + 1)){
                    tomorrowData.push(dish);
                }
            });
        }
    });
    addTableToDialog(todayData, "today-list", "#today");
    addTableToDialog(tomorrowData, "tomorrow-list", "#tomorrow");
    initIncreaseDecreaseEvents();
});

var addTable = function (data, elem, table) {
    AJS.$(table).prepend(Slimak.Templates.Dishes.table({tableId: elem}));

    _.each(data, function (dish) {
        AJS.$("#" + elem + " ." + dish.category).after(Slimak.Templates.Dishes.singleDish({dish: dish}));
    });
};

var addTableToDialog = function (data, elem, table) {
    AJS.$(table).html(Slimak.Templates.Dishes.userTable({tableId: elem}));

    _.each(data, function (dish) {
        AJS.$("#" + elem + " ." + dish.category).after(Slimak.Templates.Dishes.singleDishForUser({dish: dish}));
    });
};

var initIncreaseDecreaseEvents = function () {
    AJS.$(".increase").click(function () {
        var tr = AJS.$(this).parent().parent();
        var tmp = AJS.$(tr).find(".count-val");
        $(tmp).text( parseInt($(tmp).text()) + 1 );
        saveChanges(tr);
    });
    AJS.$(".decrease").click(function () {
        var tr = AJS.$(this).parent().parent();
        var tmp = AJS.$(tr).find(".count-val");
        if ($(tmp).text() > 0){
            $(tmp).text(parseInt($(tmp).text()) - 1 )
        }
        saveChanges(tr);
    });
};

var saveChanges = function (tr) {
    AJS.$.ajax({
        "type": "PUT",
        "url": AJS.contextPath() + "/rest/slimak/latest/Food/update",
        "contentType": "application/json; charset=UTF-8",
        "data": JSON.stringify(getDishListItem(tr)),
        "async": false
    });
};


var getDishListItem = function (trDish) {
    if (!AJS.$(trDish).hasClass("category")){
        return {
            id: AJS.$(trDish).attr("id"),
            foodCount: AJS.$(trDish).find(".count-val").text()
        };
    } else {
        return {};
    }
} ;