package pl.com.tt.slimak.rest;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.slimak.SlimakDishFacade;
import pl.com.tt.slimak.model.SlimakDish;
import pl.com.tt.slimak.model.SlimakMenu;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by kulickip on 2016-11-24.
 */
@Path("/Food")
@Scanned
public class DishController {

    private static final Logger LOG = LoggerFactory.getLogger(DishController.class.getName());
    private final TransactionTemplate transactionTemplate;
    private final SlimakDishFacade slimakDishFacade;

    @Autowired
    public DishController(@ComponentImport TransactionTemplate transactionTemplate,
                          SlimakDishFacade slimakDishFacade) {
        this.transactionTemplate = transactionTemplate;
        this.slimakDishFacade = slimakDishFacade;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/dishes")
    public Response getDishes() {
        LOG.info("Getting dishes");
        return Response.ok( new Gson().toJson(slimakDishFacade.getDishes())).build();
    }

    @POST
    @Path("/dishes")
    public Response createDish(final SlimakDish slimakDish) {
        LOG.info("Creating dish: {}", slimakDish);

        int id = slimakDishFacade.createDish(slimakDish);
        return Response.ok(id).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/menu")
    public Response createMenu(final String slimakMenu) {

        slimakDishFacade.deleteAllDishes();

        final SlimakMenu slimakModelMenu = new Gson().fromJson(slimakMenu, SlimakMenu.class);

        for(String category : slimakModelMenu.getPON().keySet()) {
            for(String dish : slimakModelMenu.getPON().get(category)) {
                slimakDishFacade.createDish(new SlimakDish(dish, "PON", category, 0));
            }
        }
        for(String category : slimakModelMenu.getWT().keySet()) {
            for(String dish : slimakModelMenu.getWT().get(category)) {
                slimakDishFacade.createDish(new SlimakDish(dish, "WT", category, 0));
            }
        }
        for(String category : slimakModelMenu.getSR().keySet()) {
            for(String dish : slimakModelMenu.getSR().get(category)) {
                slimakDishFacade.createDish(new SlimakDish(dish, "SR", category, 0));
            }
        }
        for(String category : slimakModelMenu.getCZW().keySet()) {
            for(String dish : slimakModelMenu.getCZW().get(category)) {
                slimakDishFacade.createDish(new SlimakDish(dish, "CZW", category, 0));
            }
        }
        for(String category : slimakModelMenu.getPT().keySet()) {
            for(String dish : slimakModelMenu.getPT().get(category)) {
                slimakDishFacade.createDish(new SlimakDish(dish, "PT", category, 0));
            }
        }

        return Response.ok("").build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/update")
    public Response updateDish(final String slimakDish) {
        LOG.info("updating dish {}", slimakDish);

        transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                return slimakDishFacade.update(new Gson().fromJson(slimakDish, SlimakDish.class));
            }
        });

        return Response.ok().build();
    }

    @DELETE
    @Path("/dishes/{id}")
    public Response deleteDish(@PathParam("id") int id) {
        LOG.info("deleting dish with id = ", id);

        slimakDishFacade.deleteDish(id);
        return Response.ok().build();
    }
}
