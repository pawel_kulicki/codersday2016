package pl.com.tt.slimak.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.ActiveObjectsException;
import net.java.ao.DBParam;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.slimak.aomodel.SlimakDishEntity;
import pl.com.tt.slimak.model.SlimakDish;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * Created by kulickip on 2016-11-24.
 */
@Component
public class SlimakDaoServiceImpl implements SlimakDaoService {

    private final ActiveObjects activeObjects;

    @Autowired
    public SlimakDaoServiceImpl(
            @ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    private ActiveObjects getActiveObjects() {
        activeObjects.flushAll();
        return activeObjects;
    }

    @Override
    public int createDish(SlimakDish slimakDish) {
        DBParam[] dbParams = new DBParam[] {
            new DBParam(SlimakDishEntity.NAME_COLUMN, slimakDish.getName()),
            new DBParam(SlimakDishEntity.CATEGORY_COLUMN, slimakDish.getCategory()),
            new DBParam(SlimakDishEntity.DAY_OF_WEEK_COLUMN, slimakDish.getDayOfWeek()),
            new DBParam(SlimakDishEntity.FOOD_COUNT_COLUMN, slimakDish.getFoodCount())
        };

        SlimakDishEntity entity = getActiveObjects().create(SlimakDishEntity.class, dbParams);
        entity.save();
        return entity.getID();
    }

    @Override
    public SlimakDishEntity updateDish(SlimakDish slimakDish) {
        SlimakDishEntity entity = getActiveObjects().get(SlimakDishEntity.class, slimakDish.getId());
        if (entity == null) {
            throw new ActiveObjectsException("Entity with id " + slimakDish.getId() + " doesn't exist.");
        }

        entity.setFoodCount(slimakDish.getFoodCount());
        entity.save();
        return entity;
    }

    @Override
    public List<SlimakDish> getDishes() {
        SlimakDishEntity[] entities = getActiveObjects().find(SlimakDishEntity.class);
        List<SlimakDish> slimakDishes = new ArrayList<>();

        for (SlimakDishEntity slimakDish : entities) {
            slimakDishes.add(new SlimakDish(slimakDish));
        }

        return slimakDishes;
    }

    @Override
    public void deleteDish(int id) {


    }

    @Override
    public void deleteDishes() {
        SlimakDishEntity[] entities = getActiveObjects().find(SlimakDishEntity.class);

        for (SlimakDishEntity entity :entities) {
            activeObjects.delete(entity);
        }
    }
}
