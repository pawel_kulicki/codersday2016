package pl.com.tt.slimak.dao;

import pl.com.tt.slimak.aomodel.SlimakDishEntity;
import pl.com.tt.slimak.model.SlimakDish;

import java.util.List;

/**
 * Created by kulickip on 2016-11-24.
 */
public interface SlimakDaoService {

    int createDish(SlimakDish slimakDish);
    SlimakDishEntity updateDish(SlimakDish slimakDish);

    List<SlimakDish> getDishes();
    void deleteDish(int id);
    void deleteDishes();
}
