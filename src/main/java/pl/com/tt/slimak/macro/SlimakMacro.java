package pl.com.tt.slimak.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import facebook4j.*;
import facebook4j.auth.AccessToken;

import java.util.Map;

/**
 * Created by kulickip on 2016-11-24.
 */
@Scanned
public class SlimakMacro implements Macro {

    private static final String PATH_TO_MACRO_VELOCITY = "templates/SlimakMacro.vm";

    public String execute(final Map<String, String> params, final String body, final ConversionContext context) {
        Facebook facebook = new FacebookFactory().getInstance();
        facebook.setOAuthAppId("", "");
        String accessTokenString = "EAACEdEose0cBAE6UlOZBZCQOqwlbCZBaRMExZA8PqWb7Por6SPV5YjJCDuZCUQ8i7S8JPafycn9nxA11Qju99tdlQoPkjpuTplNmSLIkycx6hLaxQ5CqRZCHMBsEWlRs4swslFxcO9lhon3PcaMsqC16W7GOQUVNnUfJdzwVyzXy3vokZCMChZC7";
        AccessToken at = new AccessToken(accessTokenString);
        facebook.setOAuthAccessToken(at);

        ResponseList<Post> feeds = null;
        try {
            ResponseList<Note> noteList = facebook.notes().getNotes("1583477958638913");

            for(Note note : noteList) {
                System.out.println("--------------------========================================================");
                System.out.println(note.getCreatedTime());
                System.out.println(note.getMessage());
            }

        } catch (FacebookException e) {
            e.printStackTrace();
        }
        return VelocityUtils.getRenderedTemplate(PATH_TO_MACRO_VELOCITY, MacroUtils.defaultVelocityContext());
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
