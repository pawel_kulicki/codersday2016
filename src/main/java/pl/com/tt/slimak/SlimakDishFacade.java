package pl.com.tt.slimak;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.tt.slimak.aomodel.SlimakDishEntity;
import pl.com.tt.slimak.dao.SlimakDaoService;
import pl.com.tt.slimak.model.SlimakDish;

import java.util.List;

/**
 * Created by kulickip on 2016-11-24.
 */
@Component
public class SlimakDishFacade {

    private final SlimakDaoService slimakDaoService;

    @Autowired
    public SlimakDishFacade(SlimakDaoService slimakDaoService) {
        this.slimakDaoService = slimakDaoService;
    }

    public int createDish(SlimakDish slimakDish) {
        int id = slimakDaoService.createDish(slimakDish);
        return id;
    }

    public void deleteDish(int id) {
        slimakDaoService.deleteDish(id);
    }

    public List<SlimakDish> getDishes() {
        return slimakDaoService.getDishes();
    }

    public void deleteAllDishes() {
        slimakDaoService.deleteDishes();
    }

    public SlimakDishEntity update(SlimakDish slimakDish) {
        return slimakDaoService.updateDish(slimakDish);
    }
}
