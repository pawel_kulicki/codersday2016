package pl.com.tt.slimak.sms;

import pl.smsapi.Client;
import pl.smsapi.api.SmsFactory;
import pl.smsapi.api.action.sms.SMSSend;
import pl.smsapi.api.response.MessageResponse;
import pl.smsapi.api.response.StatusResponse;
import pl.smsapi.exception.ClientException;

/**
 * Created by miedzianowskim on 25.11.2016.
 */
public class SMSController {


    private final static String LOGIN_TO_SMS_API = "codersday2016.apdc@gmail.com";
    private final static String PASSWORD_HASH = "5703b5d248af1d9a9fbe8eb6260ee60b";
    private static SMSController smsController = null;

    private SmsFactory smsAPI;

    private SMSController() {
        try {
            Client client = new Client(LOGIN_TO_SMS_API);
            client.setPasswordHash(PASSWORD_HASH);
            this.smsAPI = new SmsFactory(client);
        }
        catch (ClientException e) {
            System.out.println(e.getMessage());
        }

    }

    public static SMSController getInstance() {
        if (smsController == null) {
            smsController = new SMSController();
            return smsController;
        } else {
            return smsController;
        }
    }

    public boolean sendMessage(String number, String message) {
        try {

            SMSSend action = smsAPI.actionSend()
                    .setText(message)
                    .setTo(number)
                    .setSender("ECO"); //NIE ZMIENIAC!

            StatusResponse result = action.execute();

            for (MessageResponse status : result.getList()) {
                System.out.println(status.getNumber() + " " + status.getStatus());
            }
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
            /*
             * 101 Niepoprawne lub brak danych autoryzacji.
             * 102 Nieprawidłowy login lub hasło
             * 103 Brak punków dla tego użytkownika
             * 105 Błędny adres IP
             * 110 Usługa nie jest dostępna na danym koncie
             * 1000 Akcja dostępna tylko dla użytkownika głównego
             * 1001 Nieprawidłowa akcja
             */
            /* błąd po stronie servera lub problem z parsowaniem danych
             *
             * 8 - Błąd w odwołaniu
             * 666 - Wewnętrzny błąd systemu
             * 999 - Wewnętrzny błąd systemu
             * 201 - Wewnętrzny błąd systemu
             */

        }
    }
}
