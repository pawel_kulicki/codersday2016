package pl.com.tt.slimak.aomodel;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

/**
 * Created by kulickip on 2016-11-24.
 */
@Preload({
        SlimakDishEntity.NAME_COLUMN,
        SlimakDishEntity.FOOD_COUNT_COLUMN
})
@Table("SLIMAK_DISH1")
public interface SlimakDishEntity extends Entity {
    String ID_COLUMN = "ID";
    String NAME_COLUMN = "NAME";
    String DAY_OF_WEEK_COLUMN = "DAY_OF_WEEK";
    String CATEGORY_COLUMN = "CATEGORY";
    String FOOD_COUNT_COLUMN = "FOOD_COUNT";

    @NotNull
    String getName();
    void setName(String name);

    @NotNull
    String getDayOfWeek();
    void setDayOfWeek(String dayOfWeek);

    @NotNull
    String getCategory();
    void setCategory(String category);

    @NotNull
    int getFoodCount();
    void setFoodCount(int foodCount);
}
