package pl.com.tt.slimak.mail;

import com.atlassian.mail.Email;
import com.atlassian.mail.MailException;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.SMTPMailServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by kulickip on 2016-11-24.
 */
@Component
public class SlimakMailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SlimakMailService.class);
    private final SMTPMailServer mailServer = MailFactory.getServerManager().getDefaultSMTPMailServer();

    public void sendMail(String recipientEmail) {
        final String subject = "Aktualizacja menu Slimaka";
        final String body = "Zaktualizuj cotygodniowe menu Slimaka na Confluence.";
        final String recipients = recipientEmail;
        final String recipientsCc = "";

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader( javax.mail.Session.class.getClassLoader() );

        try {
            final Email email = new Email(recipients, recipientsCc, "");
            email.setSubject(subject);
            email.setBody(body);
//            email.setEncoding("charset=iso-8859-2");

            if (mailServer != null) {
                try {
                    mailServer.send(email);
                } catch (MailException e) {
                    LOGGER.error("Unexpected MailException while sending email: " + e.getLocalizedMessage());
                }
            }
        } finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

}
