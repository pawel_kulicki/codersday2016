package pl.com.tt.slimak.model;

import java.util.List;
import java.util.Map;

/**
 * Created by polkam on 25.11.2016.
 */
public class SlimakMenu {
    private Map<String, List<String>> PON;
    private Map<String, List<String>> WT;
    private Map<String, List<String>> SR;
    private Map<String, List<String>> CZW;
    private Map<String, List<String>> PT;

    public SlimakMenu(Map<String, List<String>> PON,
                      Map<String, List<String>> WT,
                      Map<String, List<String>> SR,
                      Map<String, List<String>> CZW,
                      Map<String, List<String>> PT) {
        this.PON = PON;
        this.WT = WT;
        this.SR = SR;
        this.CZW = CZW;
        this.PT = PT;
    }

    public Map<String, List<String>> getPON() {
        return PON;
    }

    public void setPON(Map<String, List<String>> PON) {
        this.PON = PON;
    }

    public Map<String, List<String>> getWT() {
        return WT;
    }

    public void setWT(Map<String, List<String>> WT) {
        this.WT = WT;
    }

    public Map<String, List<String>> getSR() {
        return SR;
    }

    public void setSR(Map<String, List<String>> SR) {
        this.SR = SR;
    }

    public Map<String, List<String>> getCZW() {
        return CZW;
    }

    public void setCZW(Map<String, List<String>> CZW) {
        this.CZW = CZW;
    }

    public Map<String, List<String>> getPT() {
        return PT;
    }

    public void setPT(Map<String, List<String>> PT) {
        this.PT = PT;
    }
}
