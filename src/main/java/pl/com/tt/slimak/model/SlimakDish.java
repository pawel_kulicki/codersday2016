package pl.com.tt.slimak.model;

import pl.com.tt.slimak.aomodel.SlimakDishEntity;

/**
 * Created by kulickip on 2016-11-24.
 */
public class SlimakDish {

    private int id;
    private String name;
    private String dayOfWeek;
    private String category;
    private int foodCount;

    public SlimakDish(String name, String dayOfWeek, String category, int count) {
        this.name = name;
        this.dayOfWeek = dayOfWeek;
        this.category = category;
        this.foodCount = count;
    }

    public SlimakDish(SlimakDishEntity slimakDishEntity) {
        this.id = slimakDishEntity.getID();
        this.name = slimakDishEntity.getName();
        this.dayOfWeek = slimakDishEntity.getDayOfWeek();
        this.category = slimakDishEntity.getCategory();
        this.foodCount = slimakDishEntity.getFoodCount();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getFoodCount() {
        return foodCount;
    }

    public void setFoodCount(int foodCount) {
        this.foodCount = foodCount;
    }
}
