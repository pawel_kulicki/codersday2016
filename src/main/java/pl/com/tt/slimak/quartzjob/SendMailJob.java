package pl.com.tt.slimak.quartzjob;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.slimak.mail.SlimakMailService;
import pl.com.tt.slimak.sms.SMSController;

/**
 * Created by kulickip on 2016-11-24.
 */
public class SendMailJob implements Job {

    private final SlimakMailService slimakMailService;

    @Autowired
    public SendMailJob(SlimakMailService slimakMailService) {
        this.slimakMailService = slimakMailService;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String recipient = "kulickip@tt.com.pl";
        slimakMailService.sendMail(recipient);
    }
}
