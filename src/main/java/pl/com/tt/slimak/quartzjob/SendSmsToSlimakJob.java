package pl.com.tt.slimak.quartzjob;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.tt.slimak.SlimakDishFacade;
import pl.com.tt.slimak.model.SlimakDish;
import pl.com.tt.slimak.sms.SMSController;

import java.util.*;

/**
 * Created by kulickip on 2016-11-25.
 */
public class SendSmsToSlimakJob implements Job {
    private static final String NUMBER = "697878925";

    private final SlimakDishFacade slimakDishFacade;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public SendSmsToSlimakJob(SlimakDishFacade slimakDishFacade, @ComponentImport TransactionTemplate transactionTemplate) {
        this.slimakDishFacade = slimakDishFacade;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
//        final Map<String, List<SlimakDish>> dishesByCategory = slimakDishFacade.getDishes().stream()
//                .filter(dish -> dish.getFoodCount() > 0)
//                .collect(Collectors.groupingBy(SlimakDish::getCategory));

        transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction() {
                StringBuilder sms = new StringBuilder();
                final Map<String, List<SlimakDish>> dishesByCategory = new HashMap<>();
                List<SlimakDish> zupy = new ArrayList<>();
                List<SlimakDish> kubek = new ArrayList<>();
                List<SlimakDish> kubekFit = new ArrayList<>();
                List<SlimakDish> salatka = new ArrayList<>();
                List<SlimakDish> deser = new ArrayList<>();
                List<SlimakDish> inne = new ArrayList<>();
                for(SlimakDish dish : slimakDishFacade.getDishes()) {
                    if(dish.getFoodCount() > 0) {
                        switch (dish.getCategory()) {
                            case "ZUPY":
                                zupy.add(dish);
                                break;
                            case "KUBEK":
                                kubek.add(dish);
                                break;
                            case "KUBEK_FIT":
                                kubekFit.add(dish);
                                break;
                            case "SALATKI":
                                salatka.add(dish);
                                break;
                            case "DESERY":
                                deser.add(dish);
                                break;
                            default:
                                inne.add(dish);
                        }
                    }
                }
                if(zupy.size() > 0) {
                    dishesByCategory.put("Zupy", zupy);
                }
                if(kubek.size() > 0) {
                    dishesByCategory.put("Kubek", kubek);
                }
                if(kubekFit.size() > 0) {
                    dishesByCategory.put("Kubek fit", kubekFit);
                }
                if(salatka.size() > 0) {
                    dishesByCategory.put("Salatki", salatka);
                }
                if(deser.size() > 0) {
                    dishesByCategory.put("Desery", deser);
                }
                if(inne.size() > 0) {
                    dishesByCategory.put("Inne", inne);
                }

                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_WEEK);

                sms.append("Zamowienie na ");
                switch (day) {
                    case Calendar.MONDAY:
                        sms.append("Poniedzialek: ");
                        break;
                    case Calendar.TUESDAY:
                        sms.append("Wtorek: ");
                        break;
                    case Calendar.WEDNESDAY:
                        sms.append("Sroda: ");
                        break;
                    case Calendar.THURSDAY:
                        sms.append("Czwartek: ");
                        break;
                    case Calendar.FRIDAY:
                        sms.append("Piatek: ");
                        break;
                }
                for(String key : dishesByCategory.keySet()) {
                    sms.append(key).append(": ");
                    for(SlimakDish dish : dishesByCategory.get(key)) {
                        if(dishesByCategory.get(key).get(dishesByCategory.get(key).size() - 1).equals(dish)) {
                            sms.append(dish.getName()).append(" x").append(dish.getFoodCount());
                        } else {
                            sms.append(dish.getName()).append(" x").append(dish.getFoodCount()).append(",");
                        }
                    }
                    sms.append("; ");
                }
                sms.append("Transition Tecnologies, ul. Piotrkowska 276");
                System.out.println("SMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSMSSM");
                System.out.println(sms);
                SMSController.getInstance().sendMessage(NUMBER, sms.toString());
                return "";
            }
        });


    }
}
